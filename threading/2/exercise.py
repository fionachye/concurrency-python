import time

from threading import Thread

MAX_NUM = 50000000

class CustomThread(Thread):

    def __init__(self, task, num1, num2):
        self.result = 0
        self.num1 = num1
        self.num2 = num2
        self.task = task
        super().__init__()

    def run(self):
        self.result = self.task(self.num1, self.num2)

def task(num1, num2):
    count = 0
    for i in range(num1, num2):
        count += i
    return count

def concurrent_run():
    start_time = time.time()
    step_size = MAX_NUM
    
    threads = []
    for i in range(0, MAX_NUM, step_size): # 0-999, 1000-1999, ...
        thread = CustomThread(task, i, min(i+step_size, MAX_NUM))
        threads.append(thread)
    
    print(f"Number of threads: {len(threads)}")
    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()
    
    result = sum([t.result for t in threads])
    total_time = time.time() - start_time
    print(f"concurrent_run: {total_time}, result: {result}")


def run():
    start_time = time.time()
    step_size = MAX_NUM
    results = []
    for i in range(0, MAX_NUM, step_size):
        results.append(task(i, min(i+step_size, MAX_NUM)))
    
    result = sum(results)
    total_time = time.time() - start_time
    print(f"run: total time: {total_time}, result: {result}")


if __name__ == "__main__":
    run()
    concurrent_run()

