import time

from threading import Thread

MAX_NUM = 10
REPEAT_COUNT = 40


def task():
    time.sleep(1)


def concurrent_run():
    start_time = time.time()
    
    threads = []
    for _ in range(REPEAT_COUNT):
        thread = Thread(target=task)
        threads.append(thread)
    
    print(f"Number of threads: {len(threads)}")
    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()
    
    total_time = time.time() - start_time
    print(f"concurrent_run: {total_time}")


def run():
    start_time = time.time()
    for _ in range(REPEAT_COUNT):
        task()
    total_time = time.time() - start_time
    print(f"run: total time: {total_time}")


if __name__ == "__main__":
    run()
    concurrent_run()

