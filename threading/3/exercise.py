import requests
import traceback
import threading
import time

from threading import Thread, current_thread


class BackgroundTaskException(Exception):
    def __init__(self, thread, error):
        self.thread = thread


def custom_hook(args):
    print(f"Exception occured in {args.thread}: \n{traceback.format_exc()}")


def background_task(search_term):
    requests.get(f"https://www.google.com/search?q={search_term}")

    time.sleep(len(search_term))
    if search_term == "error":
        raise BackgroundTaskException(current_thread(), "A search error occured")


def main():
    threading.excepthook = custom_hook

    threads = [
        Thread(
            target=background_task, args=(search_term,)
        ) for search_term in ["hi", "world", "news", "technology", "weatherrrrrrrrr", "error"]
    ]

    for thread in threads:
        thread.start()

    for thread in threads:
        print(f"Checking thread status: {thread.name}: {thread.is_alive()}")
        thread.join()
        print(f"{thread.name} joined")

if __name__ == "__main__":
    main()
