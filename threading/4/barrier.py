import time

from threading import Barrier
from threading import Thread

def adder(shared_barrier):
    global value
    for _ in range(1000000):
        tmp = value
        time.sleep(0)
        tmp = tmp + 1
        time.sleep(0)
        value = tmp
    shared_barrier.wait()
 
# make subtractions from the global variable
def subtractor(shared_barrier):
    global value
    for _ in range(1000000):
        tmp = value
        time.sleep(0)
        tmp = tmp - 1
        time.sleep(0)
        value = tmp
    shared_barrier.wait()


if __name__ == "__main__":
    value = 0
    barrier = Barrier(3, lambda: print("hello world"))
    add_thread = Thread(target=adder, args=(barrier,))
    subtract_thread = Thread(target=subtractor, args=(barrier,))

    add_thread.start()
    subtract_thread.start()

    barrier.wait()

    print(value)
 
