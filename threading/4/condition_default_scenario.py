import time

from threading import Condition
from threading import Thread

"""
Scenario 1: main() -> subtract() -> add() -> main() exits
Main thread will run.
subtract() will run next. It will then wait(), which allows other threads to acquire the condition
There are two possibilities next:
    1. If the main thread manage to acquire the condition
       - main thread will yield and wait()
       - add() thread will then acquire the condition, which enters 2.
    2. If the add() thread manage to acquire the condition
       - add() thread will execute
       - add() will notify subtract() thread
       - subtract() thread will continue and then notify() the main thread
"""

# add to the balance
def add(shared_condition):
    global balance
    # 3. Acquire the condition after subtract() releases the lock
    with shared_condition:
        print("add(): Thread acquired shared_condition and continues")
        for _ in range(1000000):
            tmp = balance
            time.sleep(0)
            tmp += 1
            time.sleep(0)
            balance = tmp
        print("add(): Thread going to notify again")
        # 4. Notifies subtract() and main thread
        shared_condition.notify_all()

# subtract from the balance
def subtract(shared_condition):
    global balance
    # 1. Acquire the condition
    with shared_condition:
        print("subtract(): Thread acquired shared_condition")
        for _ in range(1000000):
            tmp = balance
            time.sleep(0)
            tmp -= 1
            time.sleep(0)
            balance = tmp
        print("subtract(): Thread going to wait")
        # 2. Release the lock and block
        shared_condition.wait()
        # 5. wait() unblocks due to 4., reacquires lock and continue
        print("subtract(): Thread stopped waiting, going to notify..")
        # 6. Notifies main thread while still holding the condition lock
        shared_condition.notify_all()
    # 7. Finally releases the condition lock


if __name__ == "__main__":
    balance = 0
    condition = Condition()
    add_thread = Thread(target=add, args=(condition,))
    subtract_thread = Thread(target=subtract, args=(condition,))

    subtract_thread.start()
    add_thread.start()
    
    # 3. Could acquire the condition after subtract() releases the lock
    with condition:
        print("Main thread acquired shared condition")
        # 3.1. Will yield the lock and block here, allowing add() to acquire the lock
        condition.wait()
        # 8. wait() unblocks due to 6, reacquires condition lock
        print("Main thread reacquired lock after waiting")
        print(balance)
