import time

from threading import Condition
from threading import Thread

"""
Scenario 1: main() -> subtract() -> add() -> main() exits
Main thread will run.
If subtract() runs first, it will release the condition which will be acquired by add().
Likewise if add() runs first, it will wait() and release the lock, which will be acquired by
subtract(). subtract() will notify() and set its `subtracted=True`, then add() will continue
its execution.
"""

# add to the balance
def add(shared_condition):
    global balance
    global subtracted
    with shared_condition:
        print("add(): Thread acquired condition and going to notify() then wait()")
        shared_condition.wait_for(lambda: subtracted is True)
        print("add(): Thread reacquired condition from wait() and continues")
        for _ in range(1000000):
            tmp = balance
            time.sleep(0)
            tmp += 1
            time.sleep(0)
            balance = tmp

# subtract from the balance
def subtract(shared_condition):
    global balance
    global subtracted
    with shared_condition:
        print("subtract(): Thread acquired shared_condition")
        for _ in range(1000000):
            tmp = balance
            time.sleep(0)
            tmp -= 1
            time.sleep(0)
            balance = tmp
        print("subtract(): Thread going to notify()")
        # Notifies add() if add() executes first and waiting()
        subtracted = True
        shared_condition.notify()


if __name__ == "__main__":
    balance = 0
    subtracted = False
    condition = Condition()
    add_thread = Thread(target=add, args=(condition,))
    subtract_thread = Thread(target=subtract, args=(condition,))
    
    # The order of starting the threads does not matter
    subtract_thread.start()
    add_thread.start()

    for t in [add_thread, subtract_thread]:
        t.join()

    print(balance)
    
