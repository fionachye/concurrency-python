import time

from threading import Event
from threading import Thread

# add to the balance
def add(shared_event):
    global balance
    shared_event.wait()
    for _ in range(1000000):
        tmp = balance
        time.sleep(0)
        tmp += 1
        time.sleep(0)
        balance = tmp

# subtract from the balance
def subtract(shared_event):
    global balance
    shared_event.wait()
    for _ in range(1000000):
        tmp = balance
        time.sleep(0)
        tmp -= 1
        time.sleep(0)
        balance = tmp


if __name__ == "__main__":
    balance = 0

    event = Event()
    threads = [Thread(target=add, args=(event,))] + [Thread(target=subtract, args=(event,))]

    for thread in threads:
        thread.start()

    time.sleep(1)
    event.set()

    for thread in threads:
        thread.join()

    print(balance)
