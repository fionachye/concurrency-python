from threading import Lock
from threading import Thread

# add to the balance
def add(shared_lock):
    global balance
    with shared_lock:
        for i in range(1000000):
            balance += 1

# subtract from the balance
def subtract(shared_lock):
    global balance
    with shared_lock:
        for i in range(1000000):
            balance -= 1


if __name__ == "__main__":
    balance = 0
    lock = Lock()
    threads = [Thread(target=add, args=(lock,)) for _ in range(5)] + \
        [Thread(target=subtract, args=(lock,)) for _ in range(5)]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    print(balance)
