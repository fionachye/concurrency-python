import time
from threading import Thread

# add to the balance
def add():
    global balance
    for _ in range(1000000):
        tmp = balance
        time.sleep(0)
        tmp += 1
        time.sleep(0)
        balance = tmp

# subtract from the balance
def subtract():
    global balance
    for _ in range(1000000):
        tmp = balance
        time.sleep(0)
        tmp -= 1
        time.sleep(0)
        balance = tmp


if __name__ == "__main__":
    balance = 0
    threads = [Thread(target=add)] + [Thread(target=subtract)]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    print(balance)
