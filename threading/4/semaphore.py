import time

from threading import Semaphore
from threading import Thread

# add to the balance
def add(shared_semaphore):
    global balance
    with shared_semaphore:
        for _ in range(1000000):
            tmp = balance
            time.sleep(0)
            tmp += 1
            time.sleep(0)
            balance = tmp

# subtract from the balance
def subtract(shared_semaphore):
    global balance
    with shared_semaphore:
        for _ in range(1000000):
            tmp = balance
            time.sleep(0)
            tmp -= 1
            time.sleep(0)
            balance = tmp


if __name__ == "__main__":
    balance = 0

    # There are 2 threads in total, so initialize semphore with 2 or greater
    # to force a race condition
    # Semaphore(1) is equivalent to having a mutex
    semaphore = Semaphore(1)
    threads = [Thread(target=add, args=(semaphore,))] + [Thread(target=subtract, args=(semaphore,))]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    print(balance)
