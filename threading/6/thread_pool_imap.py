import time

from random import random
from multiprocessing.pool import ThreadPool


"""The `imap` method returns an iterator over the results, this means that results
can be processed as soon as they become available, without having to wait for 
all tasks to complete.

The `imap` method returns the results in the order in which the tasks were added
to the pool.
"""
def task(ident):
    num = random()
    sleep_time = num * 10
    time.sleep(sleep_time)
    print(f"Thread {ident} just done sleeping for {sleep_time}")
    return num


if __name__ == "__main__":
    with ThreadPool() as pool:
        for result in pool.imap(task, range(10)):
            print(result)
