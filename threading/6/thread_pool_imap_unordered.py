import time

from random import random
from multiprocessing.pool import ThreadPool


"""The `imap_unordered` returns the results in an arbitrary order.
The order of the results may not match the order of the tasks in the input list.
"""
def task(ident):
    num = random()
    sleep_time = num * 10
    time.sleep(sleep_time)
    print(f"Thread {ident} just done sleeping for {sleep_time}")
    return num


if __name__ == "__main__":
    with ThreadPool() as pool:
        for result in pool.imap_unordered(task, range(10)):
            print(result)
